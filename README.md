# Little Temp

This is a small project for a WiFi weather node. It uses ESP32 as 802.11 gateway and a STM32F103C4x as microcontroller.
Power is provided through a Li+ single cell battery, and charging bus using a USB B micro port.

There is no power protection to isolate the battery from the system, therefore is the MCU role to check the battery currently
voltage and shut the board off in case off low-voltage.

## CAD used

PCB and schematics were designed using KiCAD 5.0.2 on Gnome Debian. The CAD can be obtained for free at:

[https://kicad.org/download/](KiCad - Downloads)

## Handbonning capabilities

LIMITED
